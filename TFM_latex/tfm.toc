\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}Introducci\'on}{3}{section.1}%
\contentsline {section}{\numberline {2}Algunos conceptos b\'asicos de cosmolog\IeC {\'\i }a y f\IeC {\'\i }sica \\~ del CMB}{8}{section.2}%
\contentsline {section}{\numberline {3}Estudio de las distorsiones espectrales.}{12}{section.3}%
\contentsline {subsection}{\numberline {3.1}Mecanismos de termalizaci\'on}{16}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}Dispersi\'on el\'astica de Compton}{17}{subsubsection.3.1.1}%
\contentsline {subsubsection}{\numberline {3.1.2}Dispersi\'on de Compton doble}{17}{subsubsection.3.1.2}%
\contentsline {subsubsection}{\numberline {3.1.3}Bremsstrahlung}{18}{subsubsection.3.1.3}%
\contentsline {subsection}{\numberline {3.2}Tipos de distorsiones}{19}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}Distorsiones de tipo $\mu $}{20}{subsubsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2}Distorsiones de tipo $y$}{21}{subsubsection.3.2.2}%
\contentsline {subsubsection}{\numberline {3.2.3}Distorsiones de tipo intermedio $i$}{22}{subsubsection.3.2.3}%
\contentsline {subsection}{\numberline {3.3}Procesos causantes de las distorsiones.}{22}{subsection.3.3}%
\contentsline {section}{\numberline {4}Medidas de informaci\'on}{24}{section.4}%
\contentsline {section}{\numberline {5}Estudio informacional del CMB con distorsiones}{27}{section.5}%
\contentsline {subsection}{\numberline {5.1}Medidas de informaci\'on del espectro del cuerpo negro a la temperatura del CMB}{27}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Medidas de informaci\'on del espectro del CMB con distorsiones de tipo $\mu $}{29}{subsection.5.2}%
\contentsline {subsection}{\numberline {5.3}Medidas de informaci\'on del espectro del CMB con distorsiones de tipo $y$}{33}{subsection.5.3}%
\contentsline {subsection}{\numberline {5.4}Medidas entr\'opicas del espectro del CMB con distorsiones de tipo $\mu + y$}{37}{subsection.5.4}%
\contentsline {section}{\numberline {6}Medidas de informaci\'on del espectro del CMB con distorsiones creado por un PMF}{40}{section.6}%
\contentsline {subsection}{\numberline {6.1}Distorsiones $\mu $ e $y$ provocadas por amortiguamiento del PMF}{43}{subsection.6.1}%
\contentsline {subsubsection}{\numberline {6.1.1}Amortiguamiento de un PMF con \IeC {\'\i }ndice espectral $n_B=2$}{49}{subsubsection.6.1.1}%
\contentsline {subsection}{\numberline {6.2}Distorsiones $y$ provocadas por la presencia de un PMF tras la Recombinaci\'on}{52}{subsection.6.2}%
\contentsline {section}{\numberline {7}Medidas de informaci\'on del espectro del CMB con distorsiones creadas por otros mecanismos}{56}{section.7}%
\contentsline {section}{\numberline {8}Conclusiones}{59}{section.8}%
\contentsline {section}{\numberline {9}Otras medidas de informaci\'on}{61}{section.9}%
\contentsline {section}{Ap\'endices:}{64}{table.caption.60}%
\contentsline {section}{\numberline {A}Medidas de informaci\'on del n\'umero de fotones}{64}{appendix.Alph1}%
\contentsline {section}{\numberline {B}Derivadas de la densidad espectral y de las medidas entr\'opicas respecto a los par\'ametros $\mu $ e $y$}{65}{appendix.Alph2}%
\contentsline {paragraph}{Derivadas parciales de la densidad espectral respecto al par\'ametro $\mu $.}{65}{section*.62}%
\contentsline {paragraph}{Derivadas parciales de las medidas entr\'opicas respecto al par\'ametro $\mu $}{65}{section*.63}%
\contentsline {paragraph}{Medidas entr\'opicas relativas}{66}{section*.64}%
\contentsline {section}{\numberline {C}Trabajo futuro}{68}{appendix.Alph3}%
