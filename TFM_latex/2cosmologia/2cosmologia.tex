En esta secci�n presentamos algunos conceptos b�sicos del modelo cosmol�gico y de f�sica del CMB necesarios para el resto del trabajo. Las ecuaciones b�sicas del modelo cosmol�gico est�ndar vienen dictadas por las ecuaciones de campo de Einstein fijando la m�trica de Friedmann-Lema\^{i}tre-Robertson-Walker (FLRW)\cite{ryden}. En general, las distintas componentes del Universo se tratan como un fluido ideal, estando caracterizada cada componente por su ecuaci�n de estado, es decir, presi�n $p$ y densidad $\rho$.

La ecuaci�n que describe la expansi�n del Universo es la ecuaci�n de Friedmann:
\begin{equation}\label{eq:friedmann}
\frac{\dot{a}^2}{a^{2}}=\frac{8\pi G}{3}\rho-kc^2+\frac{c^2\Lambda}{3},
\end{equation}
%
\noindent siendo $a(t)$ el factor de escala del Universo; $\rho$, la densidad total (materia y radiaci�n); $c$, la velocidad de la luz; $\Lambda$, la densidad de energ�a oscura y $k$ el par�metro de la m�trica de FLRW que proporciona la curvatura del espacio. Notar que dependiendo del valor de $k$, el espacio ser� plano ($k=0$), esf�rico o cerrado ($k=-1$) o hiperb�lico o abierto ($k=1$). Para cualquier Universo con una m�trica FLRW la expansi�n o contracci�n est� gobernada por la densidad total (incluida la densidad de energ�a oscura). El factor de escala del Universo parametriza la expansi�n del mismo siendo $a(t_0)=1$, donde el sub�ndice $0$ siempre denotar� el valor de la magnitud en el presente, y se puede poner en funci�n del redshift observado $z$\footnote{El redshift es una medida de distancia cosmol�gica, basada en el corrimiento al rojo de la luz observada comparada con la luz medida en el laboratorio ($ z = \frac{\lambda_0 - \lambda}{\lambda}$).} como $a(t)=(1+z)^{-1}$. El ritmo de expansi�n del Universo viene dado por el par�metro de Hubble $H(t)=\frac{\dot{a}}{a}$. Notar que la densidad y presi�n que viene de las componentes del Universo verifican la ecuaci�n de continuidad: 
\begin{equation}
\dot{\rho} + 3H(p + \rho) = 0
\end{equation}
Otro par�metro interesante que determinar� la geometr�a y por tanto del futuro del Universo es la \emph{densidad cr�tica} $\rho_c(t)$:
\begin{equation}
\rho_c(t)=\frac{3}{8\pi G}H(t)^2.
\end{equation}
Si $\rho(t)>\rho_c(t)$ el universo est� curvado positivamente, si $\rho(t)<\rho_c(t)$ lo estar� negativamente y ser� plano para $\rho(t)=\rho_c(t)$. De acuerdo a la comparaci�n de las observaciones con el modelo cosmol�gico est�ndar, localmente estamos en un Universo plano, y por lo tanto, la densidad total de materia es $\Omega = \rho(t)/\rho_c(t) \approx  1$. El par�metro densidad puede ponerse como suma de los par�metros densidad debido a cada una de las componentes en el Universo, es decir:
\begin{equation}\label{eq:uniplano}
\Omega=\Omega_r +\Omega_m+\Omega_\Lambda = 1,
\end{equation}
donde $\Omega_i = \rho_i(t)/\rho_c(t)$. El sub�ndice $r$ denota radiaci�n, $m$ materia (bari�nica m�s oscura, i.e. $\Omega_m=\Omega_b+\Omega_{dm}$) y $\Lambda$ energ�a oscura. La ecuaci�n \eqref{eq:friedmann} puede ponerse en funci�n de los par�metros densidad asociados a cada componente del Universo como:
\begin{equation}\label{eq:hubblez}
H(z)=H_0\sqrt{\Omega_{r,0}(1+z)^4+\Omega_{m,0}(1+z)^3+\Omega_{\Lambda ,0}+(1-\Omega_0)(1+z)},
\end{equation}
donde $\Omega_{i,0}$ denota el valor del par�metro densidad de la componente $i$ en el presente. Para un Universo plano el �ltimo sumando se anular�a. Los �ltimos resultados de los par�metros cosmol�gicos aportados por el sat�lite Planck\cite{planckparameters} ser�n los que asumimos en este trabajo: 
\begin{align*}
\Omega_{b,0}&=0.0486\pm 0.0010;\quad \Omega_{\text{dm},0}=0.2589\pm 0.0057;\quad \Omega_{m,0}=0.3089\pm 0.0062\\
\Omega_{\Lambda,0}&=0.6911\pm 0.0062;\quad \rho_{c,0}=(8.62\pm 0.12)\cdot 10^{-27} \text{kg}/\text{m}^3;\quad H_0=67.74\pm 0.46~\text{km}~\text{s}^{-1}~\text{Mpc}^{-1}.
\end{align*}
Sin embargo, nuestro Universo no ha tenido los mismos valores de estos par�metros a lo largo de su evoluci�n, sino que dependen del redshift. Para ello, habr�a que utilizar las ecuaciones de estado de cada componente, las cuales ser�n diferentes para la materia no relativista y para la radiaci�n. Tampoco la temperatura del fondo c�smico de microondas ha sido constante en la evoluci�n del Universo, pues se ha ido enfriando debido a la expansi�n. En funci�n del redshift, estas magnitudes evolucionan como \footnote{La ecuaci�n para $\rho_m$ no solo es v�lida para la materia total, sino que tambi�n lo es considerando solo materia bari�nica u oscura. An�logamente, la evoluci�n de la densidad de fotones es para radiaci�n en general (para neutrinos tambi�n servir�a), pero en este trabajo solo se tendr� en cuenta los fotones.}:
\begin{equation}\label{eq:densr}
\rho_{m}(z)=\rho_{m,0}(1+z)^3;\quad \rho_\gamma(z)=\rho_{\gamma,0}(1+z)^4;\quad T_{\gamma}=T_0(1+z),
\end{equation}
donde $T_0=2.72548\pm 0.00057$ K es la temperatura del CMB actual y $\rho_{\gamma,0}=4.6\times 10^{-31}\text{kg}/\text{m}^3$. La evoluci�n de la constante de Hubble vendr�a dada por \eqref{eq:hubblez}. Para un Universo plano con constante cosmol�gica $\Lambda$ donde domina la materia, despreciando $\Omega_\gamma$ obtenemos:
\begin{equation}\label{eq:hubblezm}
H(z)=H_0\sqrt{\Omega_{m,0}(1+z)^3+(1-\Omega_{m,0})}
\end{equation} 
donde se ha tenido en cuenta \eqref{eq:uniplano}. Podemos obtener tambi�n la evoluci�n con el redshift de la densidad cr�tica:
\begin{equation}\label{eq:densczm}
\rho_c(z)=\frac{3}{8\pi G}H_0^2\left[\Omega_{m,0}(1+z)^3+(1-\Omega_{m,0})\right]
\end{equation}
En los c�lculos realizados en este trabajo se ha tenido en cuenta las evoluciones con el redshift \eqref{eq:densr}, \eqref{eq:hubblezm} y \eqref{eq:densczm}, utilizando los resultados del sat�lite Planck de los par�metros cosmol�gicos como valores prefijados.


Finalmente vamos a introducir brevemente algunas cuestiones sobre la f�sica del CMB para comprender algo m�s nuestro trabajo. La generaci�n del CMB tiene lugar durante la �poca de la Recombinaci�n, pero lo que se denomina �poca de la Recombinaci�n aglutina tres etapas diferentes:
%
\begin{itemize}
\item  En primer lugar, la �poca de la Recombinaci�n propiamente dicha, cuando la componente bari�nica del Universo pasa de estar ionizada a ser neutra, debido al enfriamiento del Universo por la expansi�n. Antes de esta �poca, la temperatura del Universo era mayor a $3000$~K, y la energ�a de los fotones era lo suficientemente alta como para prevenir la formaci�n de �tomos de hidr�geno o helio, estando el Universo formado por n�cleos de helio, hidr�geno y de electrones libres en forma de plasma. Los mecanismos de termalizaci�n aportan una interacci�n efectiva entre materia y radiaci�n, logr�ndose as� que se encontrasen en equilibrio t�rmico. As�, la distribuci�n espectral de los fotones ser�a la de un cuerpo negro.

\item En segundo lugar, la �poca del desacoplamiento de fotones, cuando �stos dejan de interactuar de forma efectiva con los electrones y por lo tanto el Universo se hace transparente. Esto es debido a que la escala de tiempo de los mecanismos de termalizaci�n se hacen mayor que la escala de tiempo del ritmo de expansi�n del Universo.  

\item Por �ltimo, la �poca de la �ltima dispersi�n, que se trata del momento en el que un fot�n t�pico del CMB llev� a cabo la �ltima dispersi�n con un electr�n, conservando la distribuci�n espectral del cuerpo negro. 
\end{itemize} 
%
Estrictamente los fotones que nos llegan del CMB son los procedentes de la �ltima dispersi�n. Esa primera luz guarda informaci�n sobre lo que sucedi� antes de su formaci�n, as� como de los procesos que han tenido lugar en su viaje hasta nuestra �poca. El CMB fue descubierto accidentalmente en 1964 por los radioastr�nomos A. Penzias y R. Wilson, como un exceso de temperatura de antena, totalmente is�tropa, a la cual no pudieron dar explicaci�n incialmente pero que luego, tras el anuncio, distintos te�ricos como R. Dicke  identificaron con la radiaci�n remanente del Big Bang predicha por G. Gamow en los a�os 40. En 1992 se lanz� el sat�lite COBE, confirmando la existencia de desviaciones de la isotrop�a en el CMB as� como el espectro de cuerpo negro a una temperatura de $T\approx 2.725$~K. La anisotrop�a de mayor escala es la llamada \emph{anisotrop�a dipolar} que es debida a la velocidad peculiar de la Tierra con respecto al CMB y no tiene relevancia en cosmolog�a. Una vez eliminada esta anisotrop�a se pueden observar fluctuaciones de temperatura de menor escala, en concreto del orden de $10^{-5}$~K. En la Figura~\ref{fig:cmb_planck} y \ref{fig:cmb_plancks} se muestra el mapa y el espectro de potencias de las anisotrop�as en temperatura, respectivamente, medido por el sat�lite Planck~\cite{planck16IX,planck16XI}.
%
\begin{figure}
\begin{minipage}[t]{0.5\linewidth}
\centering
\includegraphics[height=3.5cm]{Planck_CMB.jpg}
\caption{Mapa de las anisotrop�as del CMB \cite{planck16IX}}
\label{fig:cmb_planck}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth}
\centering
\includegraphics[height=5.5cm]{planck_ps.png}
\caption{Espectro de potencias de las anisotrop�as en temperatura\cite{planck16XI}}
\label{fig:cmb_plancks}
\end{minipage}
\end{figure}
%
En general el estudio del CMB se hace a trav�s del espectro angular de potencias de las anistrop�as observadas en el mapa. Para ello se realiza un desarrollo en arm�nicos esf�ricos de las mismas:
%
\begin{equation}
\label{eq:harmonics_expansion}
\frac{\Delta T}{T} = \sum_{\ell=0}^{+\infty} \sum_{m=-\ell}^{m=+\ell}{a_{\ell m} Y_{\ell m}(\cos \theta)},
\end{equation}
%
siendo $~a_{\ell m} = \int{\frac{\Delta T}{T} Y_{\ell m}^{*} \diff\Omega}~$ los coeficientes de la expansi�n en los arm�nicos esf�ricos $Y_{\ell m}(\cos \theta)$.
En el caso de una distribuci�n de anisotrop�as gaussiana, el espectro angular de potencias contendr� toda la informaci�n estad�stica, siendo �ste:
%
\begin{equation}
\label{eq:cl}
C_{\ell} = \frac{1}{2\ell + 1} \sum_{m=-\ell}^{+\ell}{|a_{\ell m}|^2}.
\end{equation}
%
En general el espectro angular de potencias tiene codificada la informaci�n de la inflaci�n en las grandes escalas (multipolos $\ell$ peque�os) del contenido del Universo, y de c�mo se ha difundido las estructuras en las peque�as escalas (multipolos $\ell$ grandes). Adem�s de la informaci�n en temperatura el CMB est� polarizado, y dicha polarizaci�n tambi�n presenta anisotrop�as (que son claves por ejemplo para la detecci�n de las ondas gravitacionales primordiales). Su polarizaci�n es lineal y se debe a la dispersi�n de Thomson, y aporta una valiosa fuente de informaci�n adicional y complementaria. 
%
Finalmente, cabe se�alar que existen distintos tipos de anisotrop�as. Algunos de los mecanismos que contribuyen a estas anisotrop�as en temperatura tienen lugar en el Universo reciente, como el efecto Sunyaev-Zel'dovich, debido a que los fotones del CMB se dispersan por los electrones del espacio intergal�ctico; y otros estuvieron presentes en el Universo temprano como el efecto Sachs-Wolfe, que consiste en el corrimiento al rojo o al azul en el CMB a causa de las fluctuaciones del potencial gravitatorio~\cite{ryden}. 

Actualmente la f�sica del CMB ha explotado enormemente la informaci�n procedente del espectro angular de potencias tanto de temperatura como de polarizaci�n. Pero existe una fuente adicional de informaci�n, la proporcionada por el espectro de cuerpo negro del CMB y sus desviaciones. Esto lo desarrollamos en la siguiente secci�n.
