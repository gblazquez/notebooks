\begin{thebibliography}{10}

\bibitem{riess98}
A.~G. Reiss, A.~V. Filippenko, {\em et~al.}, ``Observational evidence from
  supernovae for an acceleration universe and a cosmological constant,'' {\em
  The Astronomical Journal}, vol.~116, pp.~1009--1038, 1998.

\bibitem{perlmutter99}
S.~Perlmutter, G.~Aldering, and G.~Goldhaber, ``Measurements of ${\Omega}$ and
  ${\Lambda}$ from $42$ high-redshift supernovae,'' {\em Astrophysical
  Journal}, vol.~517, pp.~565--586, 1999.

\bibitem{hureview}
W.~Hu and S.~Dodelson, ``Cosmic microwave background anisotropies,'' {\em
  Annual Review of Astronomy and Astrophysics}, 2002.

\bibitem{chluba}
J.~Chluba, ``Science with {CMB} spectral distortions,'' {\em arXiv preprint
  arXiv:1405.6938}, 2014.

\bibitem{huscott}
W.~Hu, D.~Scott, and J.~Silk, ``Power spectrum constraints from spectral
  distortions in the cosmic microwave background,'' {\em The astrophysical
  Journal}, vol.~430, no.~1, pp.~L5--L8, 1994.

\bibitem{kogut}
A.~Kogut, D.~Fixsen, and D.~C. et~al., ``The primordial inflation explorer
  ({PIXIE}): A nulling polarimeter for cosmic microwave background
  observations,'' {\em Journal of Cosmology and Astroparticle Physics}, no.~7,
  p.~25, 2011.
\newblock [arXiv:1105.2044v1].

\bibitem{khatrisunyaev}
R.~Khatri and R.~A. Sunyaev, ``Forecasts for {CMB} $\mu$ and $i$-type spectral
  distortion constraints on the primordial power spectrum on scales $8\times
  10^4\lesssim k\lesssim 10^4 {M}pc^{-1}$ with the future pixie-like
  experiments,'' {\em Journal of Cosmology and Astroparticle Physics}, no.~6,
  p.~26, 2013.
\newblock [arXiv:1303.7212v2].

\bibitem{core}
F.~Bouchet {\em et~al.}, ``{CO}r{E}: Cosmic origins explorer. \emph{A White
  Paper},'' {\em arXiv preprint arXiv:1102.2181}, 2011.

\bibitem{kunze}
K.~E. Kunze and E.~Komatsu, ``Constraining primordial magnetic fields with
  distortions of the black-body spectrum of the cosmic microwave background:
  pre- and post-decoupling contributions,'' {\em Journal of Cosmology and
  Astroparticle Physics}, no.~1, p.~9, 2013.
\newblock [arXiv:1309.7994v1].

\bibitem{wielebinskibeck}
R.~Wielebinski and R.~Beck, {\em Cosmic Magnetic Fields}.
\newblock Springer, 2005.

\bibitem{widrow12}
L.~Widrow and D.~R. et. al., ``The first magnetic fields,'' {\em Space Science
  Review}, vol.~166, pp.~37--70, 2012.

\bibitem{planckpmf}
P.~Ade, N.~Aghanim, M.~Arnaud, {\em et~al.}, ``Planck 2015 results. {XIX}.
  {C}onstraints on primordial magnetic fields,'' {\em arXiv preprint
  arXiv:1502.01594}, 2016.

\bibitem{ryden}
B.~Ryden, {\em Introduction to Cosmology}.
\newblock Department of Astronomy, 2006.

\bibitem{planckparameters}
P.~Ade, N.~Aghanim, M.~Arnaud, M.~Ashdown, J.~Aumont, C.~Baccigalupi,
  A.~Banday, R.~Barreiro, J.~Bartlett, N.~Bartolo, {\em et~al.}, ``Planck 2015
  results. {XIII}. {C}osmological parameters,'' {\em Astronomy \&
  Astrophysics}, vol.~594, p.~A13, 2016.
\newblock [arXiv:1502.01589v3].

\bibitem{planck16IX}
R.~Adam, P.~Ade, {\em et~al.}, ``Planck 2015 results. {IX}. {D}iffuse component
  separation: Cmb maps,'' {\em Astronomy \& Astrophysics}, vol.~594, p.~A9,
  2016.

\bibitem{planck16XI}
N.~Aghanim, M.~Arnaud, {\em et~al.}, ``Planck 2015 results. {XI}. {CMB} power
  spectra, likelihoods, and robustness of parameters,'' {\em Astronomy \&
  Astrophysics}, vol.~594, p.~A11, 2016.

\bibitem{hannestad}
S.~Hannestad and J.~Madsen, ``Neutrino decoupling in the early universe,'' {\em
  Physical Review D}, vol.~52, no.~1764, 1995.

\bibitem{adams}
J.~Adams, U.~H. Danielsson, D.~Grasso, and H.~Rubinstein, ``Distortion of the
  acustic peaks in the {CMBR} due to a primordial magnetic field,'' {\em
  Physics Letters B}, vol.~388, no.~2, 1996.
\newblock [arXiv:astro-ph/9607043v2].

\bibitem{husilk}
W.~Hu and J.~Silk, ``Thermalization and spectral distortions of the cosmic
  background radiation,'' {\em Physical review D}, vol.~48, July 1993.

\bibitem{chlubasunyaev}
J.~Chluba and R.~Sunyaev, ``The evolution of {CMB} spectral distortion in the
  early universe,'' {\em Monthly Notices of the Royal Astronomical Society},
  vol.~419, no.~2, pp.~1294--1314, 2011.
\newblock [arXiv:1109.6552v1].

\bibitem{tashiro}
H.~Tashiro, ``{CMB} spectral distortions and energy release in the early
  universe,'' {\em Prog. Theor. Exp. Phs}, 2014.
\newblock 06B107.

\bibitem{rybicki}
G.~B. Rybicki and A.~P. Lightman, {\em Radiative Processes in Astrophysics}.
\newblock Wiley, 1979.

\bibitem{danesedezotti}
L.~Danese and G.~{De Zotti}, ``The relic radiation spectrum and the thermal
  history of the universe,'' {\em Rivista del Nuovo Cimento}, vol.~7, no.~3,
  1977.

\bibitem{chlubaphd}
J.~Chluba, {\em Spectral Distortion of the Cosmic Microwave Background}.
\newblock PhD thesis, LMU M\"{u}nchen, 2005.

\bibitem{danesedezotti2}
L.~Danese and G.~{de Zotti}, ``Double compton process and the spectrum of the
  microwave background,'' {\em Astron. Astrophys.}, vol.~107, pp.~39--42, 1982.

\bibitem{lightman}
A.~Lightman, ``Double compton emission in radiation dominated thermal
  plasmas,'' {\em The astrophysical Journal}, vol.~244, pp.~392--405, 1981.

\bibitem{chlubasazonov}
J.~Chluba, S.~Sazonov, and R.~Sunyaev, ``The double compton emissivity in a
  mildly relativistic thermal plasma wiwith the soft photon limit,'' {\em
  Astron. Astrophys.}, vol.~468, pp.~785--795, 2007.

\bibitem{huphd}
W.~Hu, {\em Wandering in the Background: A cosmic microwave Background
  explorer}.
\newblock PhD thesis, University of California at Berkeley, 1995.

\bibitem{khatri}
R.~Khatri and R.~A. Sunyaev, ``Beyond $y$ and $\mu$: the shape of the {CMB}
  spectral distortions in the intermediate epoch, $1.5\times 10^4\lesssim
  z\lesssim 2\times 10^5$,'' {\em Journal of Cosmology and Astroparticle
  Physics}, no.~9, p.~16, 2012.

\bibitem{unavoidable}
R.~A. Sunyaev and R.~Khatri, ``Unavoidable {CMB} spectral features and
  blackbody photosphere of our universe,'' {\em International Journal of Modern
  Physics D}, vol.~22, no.~7, 2013.
\newblock [arXiv:1302.6553v2].

\bibitem{creation}
R.~Khatri and R.~Sunyaev, ``Creation of the {CMB} spectrum: precise analytic
  solutions for the blackbody photosphere,'' {\em Journal of Cosmology and
  Astroparticle Physics}, no.~6, p.~38, 2012.

\bibitem{anewwindow}
J.~Chluba, D.~Fixsen, and M.~K. et~al., ``Spectral distortions of the cosmic
  microwave background: A new window to early universe physics,'' {\em
  nasa.gov}, 2013.

\bibitem{dehesa}
I.~V. Toranzo and J.~S. Dehesa, ``Entropy and complexity properties of the
  d-dimensional blackbody radiation,'' {\em Eur. Phys. J. D}, vol.~68, no.~10,
  p.~316, 2014.

\bibitem{corless}
R.~Corless, G.~Gonnet, D.~Hare, D.~Jeffrey, and D.~Knuth, ``On the {L}ambert
  {W} function,'' {\em Adv. Comput. Math.}, vol.~5, pp.~329--359, 1996.

\bibitem{handbook}
F.~Olver, D.~Lozier, R.~Boisvert, and C.~Clark, {\em {NIST} Handbook of
  Mathematical functions}.
\newblock Cambridge University Press, 2010.

\bibitem{zs}
A.~B. Zeldovich and R.~A. Sunyaev, ``The interaction of matter and radiation in
  a hot-model universe,'' {\em Institute of Applied Mathematics}, 1968.

\bibitem{sethisubramanian}
S.~Sethi and K.~Subramanian, ``Primordial magnetic fields in the
  post-recombination era and early reionization,'' {\em Mon. Not. R. Astron.
  Soc.}, vol.~356, pp.~778--788, 2005.

\bibitem{jedamzik98}
K.~Jedamzik, V.~Katalini{\'c}, and A.~V. Olinto, ``Damping of cosmic magnetic
  fields,'' {\em Physical Review D}, vol.~57, no.~6, p.~3264, 1998.

\bibitem{subramanian}
K.~Subramanian, ``The origin, evolution and signatures of primordial magnetic
  fields,'' {\em Reports on Progress in Physics}, vol.~79, no.~7, 2016.
\newblock [arXiv:1504.02311v2].

\bibitem{turner88}
M.~S. Turner and L.~M. Widrow, ``Inflation-produced, large-scale magnetic
  fields,'' {\em Physical Review D}, vol.~37, no.~10, p.~2743, 1988.

\bibitem{mack02}
A.~Mack, T.~Kahniashvili, and A.~Kosowsky, ``Microwave background signatures of
  a primordial stochastic magnetic field,'' {\em Physical review D}, vol.~65,
  no.~12, p.~123004, 2002.

\bibitem{battanerflorido}
E.~Battaner, E.~Florido, and J.~Jim\'{e}nez-Vicente, ``Magnetic fields and
  large scale structure in a hot universe,'' {\em Astron. Astrophys.}, no.~326,
  pp.~13--22, 1997.

\bibitem{karsten}
K.~Jedamzik, ``Limit on primordial small-scale magnetic fields from cosmic
  microwave background distortions,'' {\em Max-Planck-Institut f\"{u}r
  Astrophysik}, vol.~85, no.~4, 2000.

\bibitem{chlubakhatrisunyaev}
J.~Chluba, R.~Khatri, and R.~A. Sunyaev, ``{CMB} at 2x2 order: The dissipation
  of primordial acoustic waves and the observable part of the associated energy
  release,'' {\em Non. Not. Roy. Astron. Soc.}, vol.~425, pp.~1129--1169, 2012.

\bibitem{shu}
F.~H. Shu, {\em Physics of Astrophysics II}.
\newblock University science books, 1991.

\bibitem{jens2015}
J.~Chluba, D.~Paoletti, F.~Finelli, and J.-A. Rubino-Martin, ``Effect of
  primordial magnetic fields on the ionization history,'' {\em Monthly Notices
  of the Royal Astronomical Society}, vol.~451, no.~2, pp.~2244--2250, 2015.

\bibitem{ruiz-granados16}
B.~Ruiz-Granados, E.~Battaner, and E.~Florido, ``Searching for faraday rotation
  in cosmic microwave background polarization,'' {\em Monthly Notices of the
  Royal Astronomical Society}, vol.~460, no.~3, pp.~3089--3099, 2016.

\end{thebibliography}
